# Simple Hindley-Milner type checker

Recognizes following language:

```
expr : (let ([identifier expr]) expr)
     | (if expr expr expr)
     | (lambda (identifier) expr)
     | (expr expr)
     | integer
     | boolean
     | identifier
```

Example:

```sh
$ rlwrap racket main.rkt
Welcome to the type inferencer!
Enter an expression after the prompt.
> (lambda (f) (lambda (g) (lambda (x) (f (g x)))))
(-> (-> a1981 a1980) (-> (-> a1979 a1981) (-> a1979 a1980)))
^DGoodbye!
```
